Associated issue: https://jira.mongodb.org/browse/NODE-340

Clone this repo and `npm install`.

Edit *node_modules/connect-mongo/lib/connect-mongo.js*.  Line 160 reads:

	throw new Error('Error getting collection: ' + self.db_collection_name);

Change it to:

	throw err;

Edit *node_modules/connect-mongo/node_modules/mongodb/lib/mongodb/connection/server.js*.  Line 774 is in the function *canCheckoutReader* and it reads:

	return null

Change it to:

	return new Error("Dummy error");

Now run the app and visit http:/localhost:8080/read and the error should occur in Node.