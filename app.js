var ConnectMongo = require("connect-mongo");
var Express = require("express");
var ExpressSession = require("express-session");
var Mongoose = require("mongoose");


var url = "mongodb://localhost/oct1";
Mongoose.connect(url, function(err){
	if (err) return console.log(err);

	var store = new (ConnectMongo(ExpressSession))({
		mongoose_connection: Mongoose.connection
	});

	var middleware = ExpressSession({
		name: "oo.sid",
		secret: "octomom",
		cookie: {
			maxAge: 7889231400
		},
		store: store
	});

	var app = Express()
		.use(middleware)
		.get("/read", function(req, res){
			console.log(req.session);
			res.end(req.session.foo);
		})
		.get("/write", function(req, res){
			req.session.foo = new Date().valueOf().toString();
			res.end();
		})
		.listen(8080);
});
